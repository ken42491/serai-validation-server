# Serai Validation Server

## **Prerequisite** 

  Java 11
  Maven

## **Step 1** git clone serai-validation-server

## **Step 2** Set up config

  Normally config will be in application.properties

  But in this server, please just change the config in src/main/java/com/serai/validation/util/Contant.java

  You'll need to change 4 configs:

  - PHONE_VERIFICATION_URL
  - PHONE_API_KEY
  - EMAIL_VERIFICATION_URL
  - EMAIL_API_KEY

## **Step 3a** IDEA Build

  Install IntelJ, and directly build on it 

## **Step 3b** Docker Target Build

  Install Mavern Environment, run `mvn install` in root and you'll be able to see `validation-0.0.1-SNAPSHOT.jar`

  Pack with docker image and open the jar file
    e.g. 
      docker build -t validation .
      docker run -p 8080:8080 -t validation

  `DockerFile` will be able to find under src\main\docker

## Log Files

  API Logs will be stored under logs/verification.log