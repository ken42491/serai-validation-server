package com.serai.validation.controller;

import com.serai.validation.service.PhoneVerificationService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import com.serai.validation.util.*;

/**
 * @Desc Phone Validation API
*/
@Controller
@RequestMapping(Constant.VERSION + "/verification")
public class PhoneVerificationController {

  @ApiOperation(value = "Test phone number verification", notes = "Test with https://numverify.com")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "number", value = "Phone Number", required = true, dataType = "string", paramType = "query"),
    @ApiImplicitParam(name = "country_code", value = "Country Code", required = true, dataType = "string", paramType = "query"),
    @ApiImplicitParam(name = "format", value = "Format JSON", required = true, dataType = "string", paramType = "query", defaultValue = "1")
  })
  @CrossOrigin(origins = Constant.SITE_ORIGIN)
  @GetMapping("/phone-verify")
  public ResponseEntity phoneVerify(
    @RequestParam(required = true) String number,
    @RequestParam(required = true) String country_code,
    @RequestParam(required = false, defaultValue = "1") String format) {
      return ResponseEntity.ok(PhoneVerificationService.checkVerification(number, country_code, format));
  }
}