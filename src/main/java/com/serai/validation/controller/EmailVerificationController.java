package com.serai.validation.controller;

import com.serai.validation.service.EmailVerificationService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import com.serai.validation.util.*;

/**
 * @Desc Email Validation API
*/
@Controller
@RequestMapping(Constant.VERSION + "/verification")
public class EmailVerificationController {

  @ApiOperation(value = "Test Email verification", notes = "Test with https://mailboxlayer.com")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "email", value = "Email", required = true, dataType = "string", paramType = "query"),
    @ApiImplicitParam(name = "smtp", value = "smtp", required = true, dataType = "string", paramType = "query", defaultValue = "1"),
    @ApiImplicitParam(name = "format", value = "Format JSON", required = true, dataType = "string", paramType = "query", defaultValue = "1")
  })
  @CrossOrigin(origins = Constant.SITE_ORIGIN)
  @GetMapping("/email-verify")
  public ResponseEntity emailVerify(
    @RequestParam(required = true) String email,
    @RequestParam(required = false, defaultValue = "1") String smtp,
    @RequestParam(required = false, defaultValue = "1") String format) {
      return ResponseEntity.ok(EmailVerificationService.checkVerification(email, smtp, format));
  }
}