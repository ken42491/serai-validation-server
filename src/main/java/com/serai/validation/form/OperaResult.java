package com.serai.validation.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc Handling API Response
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OperaResult implements Serializable {
    // Result Success or Not
    private Boolean resultStatus;

    // Result Desc
    private String resultDesc;

    // Result Object
    private Map<String,Object> data = new HashMap<String,Object>();

}
