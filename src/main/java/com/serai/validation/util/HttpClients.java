package com.serai.validation.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.util.*;

/**
 * Global calling HTTP Client
*/
public class HttpClients {

	/**
	 * Post Request
	 * @param url
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public static String post(String url, JSONObject json, Map<String,String> map) {
		String result = "";
		try {
			// Post URL
			HttpPost httppost = new HttpPost(url);
			HttpClient httpClient = new DefaultHttpClient();

			// HTTP Post Target
			if (map != null && !map.isEmpty()) {
				Set<String> keys = map.keySet();
				for (Iterator<String> i = keys.iterator(); i.hasNext();) {
					String key = i.next().toString();
					httppost.addHeader(key, map.get(key));
				}
			}

			// Add Params
			StringEntity s = new StringEntity(json.toString(),"utf-8");
			s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			s.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httppost.setEntity(s);
			httppost.setHeader("Accept", "*/*");

			// Set Response
			HttpResponse response = httpClient.execute(httppost);

			// Send out Post and get the result 
			if (response.getStatusLine().getStatusCode() == 200) { // if result code = 200 means success
				result=EntityUtils.toString(response.getEntity());
			}
			httpClient.getConnectionManager().shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Get Request
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String get(String url) {
		 String result = "";
		 String str = "";
		 try{
			 // Calling Address
			 HttpGet request = new HttpGet(url); // Sending Get Request

			 // Getting Http Client Info
			 HttpClient httpClient = new DefaultHttpClient();

			 // Getting Response
			 HttpResponse response = httpClient.execute(request);

			 // Checking API Return network
			 if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				 result= EntityUtils.toString(response.getEntity(),"utf-8");
			 }
			 httpClient.getConnectionManager().shutdown();
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
    return result;
	}
}
