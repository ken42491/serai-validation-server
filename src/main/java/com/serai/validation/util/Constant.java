package com.serai.validation.util;

public class Constant {
  
  /**
    * Version Number
  */
  public static final String VERSION = "/v1";

  /**
    * Result Code for form
  */
  public static final boolean RESULT_SUCCESS = true;
  public static final boolean RESULT_FAIL = false;

  /**
    * API Key for phone verification
  */
  public static final String PHONE_VERIFICATION_URL = "http://apilayer.net/api/validate";
  public static final String PHONE_API_KEY = "7c52cec286e6f35c6345edef68ae488f";

  /**
    * API Key for email verification
  */
  public static final String EMAIL_VERIFICATION_URL = "http://apilayer.net/api/check";
  public static final String EMAIL_API_KEY = "a1d07821dc7659449001192f427f79a8";

  /**
    * Allow site origin
  */
  public static final String SITE_ORIGIN = "http://localhost:3000";
}