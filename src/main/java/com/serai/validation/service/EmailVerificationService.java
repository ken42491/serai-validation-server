package com.serai.validation.service;

import com.alibaba.fastjson.JSONObject;
import com.serai.validation.ValidationApplication;
import com.serai.validation.form.OperaResult;
import com.serai.validation.util.*;
import org.apache.commons.lang3.StringUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Slf4j
public class EmailVerificationService {

  private static final Logger logger = LogManager.getLogger(ValidationApplication.class);

  public static OperaResult checkVerification(String email, String smtp, String format) {
    OperaResult result = new OperaResult( );

    // Start Verification
    String verifyEmailResult = verifyEmail(Constant.EMAIL_API_KEY, email, smtp, format);
    if (!StringUtils.isEmpty(verifyEmailResult)) {
      JSONObject data = new JSONObject(true);
      data = JSONObject.parseObject(verifyEmailResult);

      // if getting error on verification
      if ("false".equals(data.getString("success"))) {

        JSONObject error = data.getJSONObject("error");
        String errorType = error.getString("type");

        result.setResultStatus(Constant.RESULT_FAIL);

        // save to log
        logger.error("Fail on Email verification ===== Email ===== " + email);
        logger.error("Fail on Email verification ===== Fail Result ===== " + verifyEmailResult);

        return result;
      }

      // Success result code
      result.setResultStatus(Constant.RESULT_SUCCESS);
      result.setResultDesc("email_is_verified");
      result.getData().put("result", data);

      // save to log
      logger.info("Success on email verification ===== " + verifyEmailResult);

    }

    return result;

  }

  /**
   * Calling external API to verify email
   * @param access_key
   * @param email
   * @param smtp
   * @param format
   * @return
   */
  public static String verifyEmail (String access_key, String email, String smtp, String format){
    String url = Constant.EMAIL_VERIFICATION_URL + "?access_key=" + access_key + "&email=" + email + "&smtp=" + smtp + "&format=" + format;
    String httpResult = HttpClients.get(url);
    return httpResult;
  }

}