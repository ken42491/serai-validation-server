package com.serai.validation.service;

import com.alibaba.fastjson.JSONObject;
import com.serai.validation.ValidationApplication;
import com.serai.validation.form.OperaResult;
import com.serai.validation.util.*;
import org.apache.commons.lang3.StringUtils;
import lombok.extern.slf4j.Slf4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Slf4j
public class PhoneVerificationService {

  private static final Logger logger = LogManager.getLogger(ValidationApplication.class);

  public static OperaResult checkVerification(String number, String country_code, String format) {
    OperaResult result = new OperaResult( );

    // Start Verification
    String verifyPhoneResult = verifyPhone(Constant.PHONE_API_KEY, number, country_code, format);
    if (!StringUtils.isEmpty(verifyPhoneResult)) {
      JSONObject data = new JSONObject(true);
      data = JSONObject.parseObject(verifyPhoneResult);

      // if getting error on verification
      if ("false".equals(data.getString("success"))) {

        JSONObject error = data.getJSONObject("error");
        String errorType = error.getString("type");

        result.setResultStatus(Constant.RESULT_FAIL);

        // if contain error type - otherwise will show general error
        if (data.containsKey("error") && !StringUtils.isEmpty(errorType)) {
          result.setResultDesc(errorType);
        } else {
          result.setResultDesc("general_error");
        }

        result.getData().put("result", error);

        // save to log
        logger.error("Fail on phone verification ===== Country Code ===== " + country_code);
        logger.error("Fail on phone verification ===== Number ===== " + number);
        logger.error("Fail on phone verification ===== Fail Result ===== " + verifyPhoneResult);

        return result;
      }

      // Success result code
      result.setResultStatus(Constant.RESULT_SUCCESS);
      result.setResultDesc("phone_is_verified");
      result.getData().put("result", data);

      // save to log
      logger.info("Success on phone verification ===== " + verifyPhoneResult);

    }

    return result;

  }

  /**
   * Calling external API to verify phone
   * @param access_key
   * @param number
   * @param country_code
   * @param format
   * @return
   */
  public static String verifyPhone (String access_key, String number, String country_code, String format){
    String url = Constant.PHONE_VERIFICATION_URL + "?access_key=" + access_key + "&number=" + number + "&country_code=" + country_code + "&format=" + format;
    String httpResult = HttpClients.get(url);
    return httpResult;
  }

}